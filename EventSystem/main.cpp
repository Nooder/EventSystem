#include <iostream>

#include "manager.h"

class InputHandler : public EventHandler<MouseEvent, KeyboardEvent>
{
	void handle(const MouseEvent& e) override {
		std::cout << "Mouse event listened: " << e.family << std::endl;
	}

	void handle(const KeyboardEvent& e) override {
		std::cout << "Keyboard event listened: " << e.family << std::endl;
	}
};

int main()
{
	EventManager manager;
	InputHandler handler;

	// Add subscribers
	manager.addHandler<MouseEvent>(&handler);
	manager.addHandler<KeyboardEvent>(&handler);

	auto a1 = manager.pending();

	// Queues events by reference
	manager.push(MouseEvent());
	manager.push(KeyboardEvent());

	auto b1 = manager.pending();
	auto b2 = manager.pending<MouseEvent>();
	auto b3 = manager.pending<KeyboardEvent>();

	// Immediatelly publishes an event by reference
	manager.publish(MouseEvent());
	manager.publish(KeyboardEvent());

	auto c1 = manager.pending();
	auto c2 = manager.pending<MouseEvent>();
	auto c3 = manager.pending<KeyboardEvent>();

	// Immediatelly publishes pushed events of a given type
	manager.publish<MouseEvent>();

	auto d1 = manager.pending();
	auto d2 = manager.pending<MouseEvent>();
	auto d3 = manager.pending<KeyboardEvent>();

	// Publishes all previously remaining pushed events (KeyboardEvent)
	manager.publish();

	auto e1 = manager.pending();
	auto e2 = manager.pending<MouseEvent>();
	auto e3 = manager.pending<KeyboardEvent>();

	return getchar();
}