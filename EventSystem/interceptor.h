#ifndef EVENT_SYSTEM_INTERCEPTOR_H
#define EVENT_SYSTEM_INTERCEPTOR_H

#include <functional>
#include <vector>

#include "event.h"

struct EventInterceptor
{
	enum Type {
		POST,
		PRE
	};

	void before(const Event& e) const {
		for (auto& handler : onBefore) {
			handler(e);
		}
	}

	void after(const Event& e) const {
		for (auto& handler : onAfter) {
			handler(e);
		}
	}

	std::vector<std::function<void(const Event&)>> onBefore;
	std::vector<std::function<void(const Event&)>> onAfter;
};

#endif