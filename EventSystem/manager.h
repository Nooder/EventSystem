#ifndef EVENT_SYSTEM_MANAGER_H
#define EVENT_SYSTEM_MANAGER_H

#include <unordered_map>
#include <typeindex>
#include <memory>

#include "channel.h"
#include "handler.h"
#include "interceptor.h"

class EventManager
{
public:
	// TEST: could be renamed to addHandler (templateless)
	void addInterceptor(EventInterceptor::Type type, std::function<void(const Event&)>&& fn) {
		switch (type) {
			case EventInterceptor::PRE:
				interceptor.onBefore.push_back(std::move(fn));
				break;
			case EventInterceptor::POST:
				interceptor.onAfter.push_back(std::move(fn));
				break;
		}
	}

	// Adds listeners to events of a given type
	template <typename T, typename = typename std::enable_if<std::is_base_of<Event, T>::value>::type>
	void addHandler(const EventHandler<T>* handler) {
		channels[typeid(T)].add([=](const Event& e) {
			const_cast<EventHandler<T>*>(handler)->handle(dynamic_cast<const T&>(e));
		});
	}

	// Checks whether there are any pending events to be published
	bool pending() {
		bool pending = false;

		for (auto& pair : channels) {
			pending |= pair.second.pending();
		}

		return pending;
	}

	// Checks whether there are pending events of a given type to be published
	template <typename T, typename = typename std::enable_if<std::is_base_of<Event, T>::value>::type>
	bool pending() {
		return channels[typeid(T)].pending();
	}

	// Publishes every queued events of all types
	void publish() {
		for (auto& pair : channels) {
			pair.second.publish(interceptor);
		}
	}

	// Publishes every queued events of a given type
	template <typename T, typename = typename std::enable_if<std::is_base_of<Event, T>::value>::type>
	void publish() {
		channels[typeid(T)].publish(interceptor);
	}

	// Immediatelly publishes an event
	void publish(const Event& e) {
		channels[typeid(e)].publish(interceptor, e);
	}

	// Queues an event
	void push(const Event& e) {
		channels[typeid(e)].push(e);
	}

private:
	EventInterceptor interceptor;
	std::unordered_map<std::type_index, Channel> channels;
};

#endif