#ifndef EVENT_SYSTEM_CHANNEL_H
#define EVENT_SYSTEM_CHANNEL_H

#include <functional>
#include <vector>
#include <queue>

#include "interceptor.h"
#include "event.h"

class Channel
{
public:
	// Adds an event dispatcher
	void add(std::function<void(const Event&)>&& fn) {
		subscribers.push_back(std::move(fn));
	}

	// Publishes every queued events
	void publish(const EventInterceptor& interceptor) {
		for (auto& wrapper : events) {
			publish(interceptor, wrapper.get());
		}

		events.clear();
	}

	// Immediatelly publishes an event
	void publish(const EventInterceptor& interceptor, const Event& e) {
		interceptor.before(e);

		for (auto& handler : subscribers) {
			handler(e);
		}

		interceptor.after(e);
	}

	// Queues an event
	void push(const Event& e) {
		events.emplace_back(e);
	}

	// Checks whether there are pending events to be published
	bool pending() {
		return !events.empty();
	}

private:
	std::vector<std::reference_wrapper<const Event>> events;
	std::vector<std::function<void(const Event&)>> subscribers;
};

#endif