#ifndef EVENT_SYSTEM_EVENt_H
#define EVENT_SYSTEM_EVENt_H

#include <cstdint>

// This abstract class provides an unique identifier if, and only if, the family value does not repeat.
// This has a base size of 4 bytes (16 + 16 bits) and enables 0xffffffff (0xffff x 0xffff) combinations.
struct Event
{
	virtual ~Event() = default;
	explicit Event(std::uint16_t family, std::uint16_t id)
		: id(id)
		, family(family)
	{}

	// The unique identifier is computed only if requested, saving some bytes
	const std::uint32_t uid() const {
		return family << 16 | id;
	}

	const std::uint16_t id;
	const std::uint16_t family;
};

// This abstract class provides an unique identifier if, and only if, the family value does not repeat.
// This has a base size of 2 bytes (8 + 8 bits) and enables 0xffff (0xff x 0xff) different event types,
// although the unique identifier is only computed when requested.
struct Event2
{
	virtual ~Event2() = default;
	explicit Event2(std::uint8_t family, std::uint8_t id)
		: id(id)
		, family(family)
	{}

	// Computes the unique identifier when requested
	const std::int16_t uid() const {
		return family << 8 | id;
	}

	const std::uint8_t id;
	const std::uint8_t family;
};

struct MouseEvent : public Event
{
	MouseEvent() : Event(0U, 0U) {}
};

struct KeyboardEvent : public Event
{
	KeyboardEvent() : Event(1U, 0U) {}
};

#endif