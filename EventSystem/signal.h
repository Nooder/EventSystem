#pragma once

#include <functional>
#include <vector>
#include <memory>

struct Connection
{
	explicit Connection(std::shared_ptr<std::function<void(unsigned)>> disconnector) : disconnector(disconnector) {}

	void disconnect() {
		if (auto lock = disconnector.lock()) {
			lock->operator()(index);
		}
	}

	std::uint32_t index;
	std::weak_ptr<std::function<void(unsigned)>> disconnector;
};

template <typename...>
class Signal;

template <typename R, typename... A>
class Signal<R(A...)>
{
public:
	template <typename T>
	Connection connect(T&& slot) {
		slots.push_back(std::forward<T>(slot));
		return Connection(disconnector);
	}

	void operator()(A&&... args) {
		for (auto& slot : slots) {
			slot(std::forward<A>(args)...);
		}
	}

private:
	std::vector<std::function<R(A...)>> slots;
	std::shared_ptr<std::function<void(unsigned)>> disconnector = std::make_shared<std::function<void(unsigned)>>([&](unsigned) {
		std::cout << "Signal::Disconnect" << std::endl;
	});
};