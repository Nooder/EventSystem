#ifndef EVENT_SYSTEM_HANDLER_H
#define EVENT_SYSTEM_HANDLER_H

#include <type_traits>

#include "event.h"

template <typename...>
class EventHandler;

template <typename T>
class EventHandler<T>
{
public:
	virtual void handle(const T&) = 0;
};

template <typename T, typename... Tn>
class EventHandler<T, Tn...> : public EventHandler<T>, public EventHandler<Tn>...
{
};

#endif